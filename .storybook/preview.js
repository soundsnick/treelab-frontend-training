import 'bootstrap-4-grid';

import { configure } from '@storybook/react'
import { addParameters } from '@storybook/client-api';

function loadStories () {
  document.body.style.fontFamily = 'Open Sans';
  document.body.style.boxSizing = 'border-box';
  document.body.style.margin = '0';
}

export const parameters = {
  controls: { expanded: true },
};

configure(loadStories, module);
addParameters({
  viewMode: 'docs',
});
