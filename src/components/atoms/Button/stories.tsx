import React, { FC, useState } from 'react';

import { Button } from './component';

export default {
  title: 'atoms/Buttons/Button',
};

export const AppearanceTypes: FC = () => {
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(false);

  const handleCounter = (): void => setCount(count + 1);
  const handleLoading = (): void => setLoading((prev) => !prev);

  return (
    <>
      <h2 className={'mt-2 mb-2'}>Style</h2>
      <Button className="mr-2" onClick={handleCounter}>
        Default ({count})
      </Button>
      <Button className="mr-2" onClick={handleCounter} appearance={'primary'}>
        Primary ({count})
      </Button>
      <Button className="mr-2" onClick={handleCounter} appearance={'translucent'}>
        Translucent ({count})
      </Button>
      <Button className="mr-2" onClick={handleCounter} appearance={'text'}>
        Text ({count})
      </Button>
      <Button className="mr-2" onClick={handleCounter} appearance={'link'}>
        Link
      </Button>

      <h2 className={'mt-4 mb-2'}>Rounded</h2>
      <Button className="mr-2" onClick={handleCounter} rounded>
        Rounded ({count})
      </Button>

      <h2 className={'mt-4 mb-2'}>
        Loading <input type={'checkbox'} onClick={handleLoading} />
      </h2>
      <Button className="mr-2" loading={loading}>
        Some content
      </Button>
      <Button className="mr-2" loading={loading} rounded>
        Loaded content
      </Button>

      <h2 className={'mt-4 mb-2'}>Disabled</h2>
      <Button className="mr-2" disabled>
        Disabled
      </Button>

      <h2 className={'mt-4 mb-2'}>Block</h2>
      <Button className="mr-2" block>
        Block
      </Button>
    </>
  );
};
