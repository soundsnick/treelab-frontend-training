export enum Brand {
  MainColor = '#4B4696',
  MainColorDark = '#2D2A5A',
  Violet14 = '#B7B5D5',
  Violet15 = '#C9C8E0',
  Violet17 = '#FAFAFF',
  Violet18 = '#FAFAFF',
  White = '#FFF',
  Error = '#FF3333',
  Yellow = '#FDC910',
}

export enum Background {
  White = '#FFF',
  Overlay = 'rgba(0, 0, 0, 0.5)',
  Transparent = 'transparent',
}

export enum Text {
  Light = '#FFF',
  Dark = '#282828',
  MainBlack = '#0F0E1E',
  GreySecondary = '#BCBCBC',
  GreySlight = '#dddddd',
}

export type ColorProp = Brand | Background | Text;
