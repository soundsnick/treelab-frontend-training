export enum Size {
  SmallText = 14,
  RegularText = 16,
  LargeText = 18,
  HugeText = 24,

  SmallHeading = 20,
  RegularHeading = 22,
  LargeHeading = 26,
}
