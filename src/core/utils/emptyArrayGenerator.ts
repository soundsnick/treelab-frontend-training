export const emptyArrayGenerator = (length: number): ReadonlyArray<number> =>
  Array.from(Array(length).keys());
